watch('(.*/*).js')  { |m| code_changed(m[0]) }
watch('(.*/*).html')  { |m| code_changed(m[1]) }


def code_changed(file)
	# this is where you put your test files (use the whole path on your HD including leading and trailing slashes)
	
	#note: spaces and special characters must be escaped for terminal!
	testsDirectory = "/Users/stevenmcconnon/Sites/Qunit\ Watchr\ Sample/example/tests/"
	
	#spaces and special charscters must be URL encoded
	apacheTestsDirectory = "http://localhost/Qunit%20Watchr%20Sample/example/tests/"
	
	
#############################################
#	nothing below here needs to be changed  #
#############################################	
	Dir.glob("#{testsDirectory}*.html").sort.each do |f|
  		fileName = File.basename(f)
  		
  		puts "\033[1mRunning #{fileName}...\033[0m"

  		run "phantomjs /bin/qunitScanner.js #{apacheTestsDirectory}#{fileName}"
  		
	end
	
	puts "\n\n\n"
end

def run(cmd)
    result = `#{cmd}`
    growl result rescue nil
end

def growl(message)
    puts(message)
    messageSplit = message.split("\n").last(2);
    growlnotify = `which growlnotify`.chomp

    title = messageSplit.find { |e| /0 failed/ =~ e } ? "PASS" : "FAILURES"
    if title == "FAILURES"
        image = "~/.watchr_images/failed.png"
        info = messageSplit[1] + "\n" + messageSplit[0]
        
    else
        image = "~/.watchr_images/passed.png"
        info = messageSplit[1] + "\n" + messageSplit[0]
    end

    options = "-w -n Watchr --image '#{File.expand_path(image)}' --html '#{title}'  -m '#{info}'"
    system %(#{growlnotify} #{options} &)
end