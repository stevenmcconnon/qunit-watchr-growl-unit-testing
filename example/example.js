//always true
function alwaysTrue () {
	return true;				
}

//always false
function alwaysFalse () {
	return false;
}


//returns whatever is passed in
function returnParam (param) {
	return param;
}