var system = require('system');

/**
 * Wait until the test condition is true or a timeout occurs. Useful for waiting
 * on a server response or for a ui change (fadeIn, etc.) to occur.
 *
 * @param testFx javascript condition that evaluates to a boolean,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param onReady what to do when testFx condition is fulfilled,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param timeOutMillis the max amount of time to wait. If not specified, 3 sec is used.
 */
function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3001, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor()' timeout");
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    //console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 100); //< repeat check every 250ms
};


if (system.args.length !== 2) {
    console.log('Usage: run-qunit.js URL');
    phantom.exit(1);
}

var page = require('webpage').create();

// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
page.onConsoleMessage = function(msg) {
    console.log(msg);
};


page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js", function() {
    // jQuery is loaded, now manipulate the DOM
    page.open(system.args[1], function(status){
	    if (status !== "success") {
	        console.log("Unable to access network");
	        phantom.exit(1);
	    } else {
	        waitFor(function(){
	            return page.evaluate(function(){
	                var el = document.getElementById('qunit-testresult');
	                if (el && el.innerText.match('completed')) {
	                    return true;
	                }
	                return false;
	            });
	        }, function(){
	            var failedNum = page.evaluate(function(){
	                var el = document.getElementById('qunit-testresult');
	                
	               
					
					$('#qunit-tests > li').each(function(i) {
	                		if($(this).has("strong") && $(this).hasClass("pass")) {
	                			console.log("   " + (i+1) + ". " + $("strong", this).text() + "…PASSED");
	                		} else if($(this).has("strong") && $(this).hasClass("fail")) {
	                			console.log("   " + (i+1) + ". " + $("strong", this).text() + "…FAILED");
	                		}
	                		
	                		$(this).find("ol > li").each(function(i){
	                			if($(this).html().indexOf("table") < 0 && $(this).html().indexOf("okay") < 0) {
	                				if($(this).hasClass("pass") && $(this).text().indexOf("okay") < 0)
										console.log("      \033[32m√ \033[0m" + $(this).text());
									else if($(this).hasClass("fail") && $(this).text().indexOf("okay") < 0)
										console.log("      \033[31mx \033[0m" + $(this).text());
	                			} else if ($(this).html().indexOf("table") >= 0) {
	                				console.log("      \033[31mx \033[0m" + $(".test-message", this).text());
	                				
	                				$(this).find("tr").each(function() {
	                					console.log("         " + $(this).text() + "");
	                				});
	          
	                			}
									
	                		});
	                		
					});
						                
	                console.log(el.innerText);
	                try {
	                    return el.getElementsByClassName('failed')[0].innerHTML;
	                } catch (e) { }
	                return 10000;
	            });
	            phantom.exit((parseInt(failedNum, 10) > 0) ? 1 : 0);
	        });
	    }
	});

});
